<?php

use App\Http\Controllers\Mobile\Auth\LoginController;
use App\Http\Controllers\Mobile\Auth\RegisterController;
use App\Http\Controllers\Mobile\ContactController;
use App\Http\Controllers\Mobile\EventController;
use App\Http\Controllers\Mobile\NoteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Mobile'], function () {

    Route::group(['middleware' => 'guest:api'], function () {
        Route::post('login', [LoginController::class, 'login'])->name('auth.login');
        Route::post('register', [RegisterController::class, 'register'])->name('auth.register');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', [LoginController::class, 'logout'])->name('auth.logout');

        Route::get('events', [EventController::class, 'index'])->name('events.index');
        Route::post('events', [EventController::class, 'store'])->name('events.store');
        Route::delete('events/{event}', [EventController::class, 'delete'])->name('events.delete');

        Route::get('notes', [NoteController::class, 'index'])->name('notes.index');
        Route::post('notes', [NoteController::class, 'store'])->name('notes.store');
        Route::delete('notes/{note}', [NoteController::class, 'delete'])->name('notes.delete');

        Route::get('contacts', [ContactController::class, 'index'])->name('contacts.index');
    });
});
