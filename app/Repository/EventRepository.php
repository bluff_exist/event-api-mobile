<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Models\Event;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class EventRepository
 */
class EventRepository
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Event::all();
    }

    /**
     * @param array $data
     * @return Event
     */
    public function create(array $data): Event
    {
        $event = new Event($data);
        $event->created_by = Auth::id();
        $event->save();

        return $event;
    }

    /**
     * @param Event $event
     * @return void
     */
    public function delete(Event $event): void
    {
        $event->delete();
    }
}
