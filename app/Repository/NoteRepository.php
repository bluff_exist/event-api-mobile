<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Models\Note;
use Illuminate\Support\Collection;

/**
 * Class NoteRepository
 */
class NoteRepository
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Note::all();
    }

    /**
     * @param array $data
     * @return Note
     */
    public function create(array $data): Note
    {
        return Note::create($data);
    }

    /**
     * @param Note $note
     * @return void
     */
    public function delete(Note $note): void
    {
        $note->delete();
    }
}
