<?php

declare(strict_types=1);

namespace App\Http\Requests\Mobile\Notes;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreNoteRequest
 */
class StoreNoteRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:100'],
            'text' => ['required', 'string', 'max:255'],
        ];
    }
}
