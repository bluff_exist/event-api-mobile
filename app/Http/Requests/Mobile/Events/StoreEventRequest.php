<?php

declare(strict_types=1);

namespace App\Http\Requests\Mobile\Events;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreEventRequest
 */
class StoreEventRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:100'],
            'date' => ['required', 'date_format:Y-m-d'],
            'description' => ['required', 'string', 'max:255'],
        ];
    }
}
