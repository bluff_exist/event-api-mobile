<?php

declare(strict_types = 1);

namespace App\Http\Requests\Mobile\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Mobile\Auth
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone' => 'required|unique:App\Models\User,phone',
            'password' => 'required|string|min:' . User::PASSWORD_MIN_LENGTH . '|max:' . User::PASSWORD_MAX_LENGTH,
            'date_of_birth' => 'required|string',
            'name' => 'required|alpha|min:2|max:255',
            'surname' => 'required|alpha|min:2|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'phone.unique' => 'Пользователь с таким номером уже зарегистрирован',

            'password.min' => 'Ваш пароль должен быть от ' . User::PASSWORD_MIN_LENGTH . ' до ' . User::PASSWORD_MAX_LENGTH . ' символов длиной',
            'password.max' => 'Ваш пароль должен быть от ' . User::PASSWORD_MIN_LENGTH . ' до ' . User::PASSWORD_MAX_LENGTH . ' символов длиной',
        ];
    }
}
