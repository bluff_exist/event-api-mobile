<?php

declare(strict_types=1);

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * Class ContactController
 */
class ContactController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            ['name' => 'test 1', 'phone' => '3809811111111'],
            ['name' => 'test 2', 'phone' => '3809811111111'],
            ['name' => 'test 2', 'phone' => '3809811111111'],
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function store(): JsonResponse
    {
        return response()->json();
    }

    /**
     * @return JsonResponse
     */
    public function delete(): JsonResponse
    {
        return response()->json();
    }
}
