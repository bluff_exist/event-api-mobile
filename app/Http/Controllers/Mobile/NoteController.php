<?php

declare(strict_types=1);

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mobile\Notes\StoreNoteRequest;
use App\Models\Note;
use App\Services\NoteService;
use Illuminate\Http\JsonResponse;

/**
 * Class NoteController
 */
class NoteController extends Controller
{
    /**
     * @var NoteService
     */
    private NoteService $noteService;

    /**
     * @param NoteService $noteService
     */
    public function __construct(NoteService $noteService)
    {
        $this->noteService = $noteService;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->noteService->getNotes());
    }

    /**
     * @param StoreNoteRequest $request
     * @return JsonResponse
     */
    public function store(StoreNoteRequest $request): JsonResponse
    {
        $note = $this->noteService->createNote($request->validated());

        return response()->json($note);
    }

    /**
     * @param Note $note
     * @return JsonResponse
     */
    public function delete(Note $note): JsonResponse
    {
        $this->noteService->deleteNote($note);

        return response()->json();
    }
}
