<?php

declare(strict_types=1);

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mobile\Events\StoreEventRequest;
use App\Models\Event;
use App\Services\EventService;
use Illuminate\Http\JsonResponse;

/**
 * Class EventController
 */
class EventController extends Controller
{
    /**
     * @var EventService
     */
    private EventService $eventService;

    /**
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->eventService->getEvents());
    }

    /**
     * @param StoreEventRequest $request
     * @return JsonResponse
     */
    public function store(StoreEventRequest $request): JsonResponse
    {
        $event = $this->eventService->createEvent($request->validated());

        return response()->json($event);
    }

    /**
     * @param Event $event
     * @return JsonResponse
     */
    public function delete(Event $event): JsonResponse
    {
        $this->eventService->deleteEvent($event);

        return response()->json();
    }
}
