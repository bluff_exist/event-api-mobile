<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Mobile\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;
use Illuminate\Http\{
    Request,
    JsonResponse
};

/**
 * Class LoginController
 */
class LoginController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        try {
            if (!Auth::attempt($request->only('phone', 'password'))) {
                return getErrors(['password' => ['Неверный номер телефона или пароль']], 401);
            }
        } catch (Exception $exception) {
            Log::info('Error:'. $exception->getMessage());
        }

        return getSuccessTokenResponse(Auth::user());
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return getSuccessResponse();
    }
}
