<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Mobile\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mobile\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;

/**
 * Class RegisterController
 */
class RegisterController extends Controller
{
    /**
     * @param RegisterRequest $request
     *
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = User::create($request->only(['phone', 'password', 'date_of_birth', 'name', 'surname']));

        return getSuccessTokenResponse($user->createToken('appApiToken'));
    }
}
