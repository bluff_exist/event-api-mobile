<?php

declare(strict_types = 1);

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;

/**
 * @param array $errors
 * @param int $code
 *
 * @return JsonResponse
 */
function getErrors($errors = [], int $code = 400): JsonResponse
{
    return Response::json(['errors' => $errors], $code, [], JSON_UNESCAPED_UNICODE);
}

/**
 * @param array $data
 * @param int $code
 *
 * @return JsonResponse
 */
function getSuccessResponse(array $data = ['success' => true], $code = 200): JsonResponse
{
    return Response::json($data, $code, [], JSON_UNESCAPED_UNICODE + defined('JSON_INVALID_UTF8_IGNORE') ? 1048576 : 0);
}

/**
 * @param User $user
 * @return JsonResponse
 */
function getSuccessTokenResponse(User $user): JsonResponse
{
    $token = $user->createToken('appApiToken');

    return getSuccessResponse([
        'token_type' => 'Bearer',
        'token' => $token->accessToken,
        'user' => $user,
        'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
    ]);
}
