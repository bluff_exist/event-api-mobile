<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Note;
use App\Repository\NoteRepository;
use Illuminate\Support\Collection;

/**
 * Class NoteService
 */
class NoteService
{
    /**
     * @var NoteRepository
     */
    protected NoteRepository $noteRepository;

    /**
     * @param NoteRepository $noteRepository
     */
    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    /**
     * @return Collection
     */
    public function getNotes(): Collection
    {
        return $this->noteRepository->all();
    }

    /**
     * @param array $data
     * @return Note
     */
    public function createNote(array $data): Note
    {
        return $this->noteRepository->create($data);
    }

    /**
     * @param Note $note
     * @return void
     */
    public function deleteNote(Note $note): void
    {
        $this->noteRepository->delete($note);
    }
}
