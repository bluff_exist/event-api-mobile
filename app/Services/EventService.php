<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Event;
use App\Repository\EventRepository;
use Illuminate\Support\Collection;

/**
 * Class EventService
 */
class EventService
{
    /**
     * @var EventRepository
     */
    protected EventRepository $eventRepository;

    /**
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return Collection
     */
    public function getEvents(): Collection
    {
        return $this->eventRepository->all();
    }

    /**
     * @param array $data
     * @return Event
     */
    public function createEvent(array $data): Event
    {
        return $this->eventRepository->create($data);
    }

    /**
     * @param Event $event
     * @return void
     */
    public function deleteEvent(Event $event): void
    {
        $this->eventRepository->delete($event);
    }
}
